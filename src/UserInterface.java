import filemanipulation.*;
import java.util.Scanner;

public class UserInterface{

  public static void main(String[] args){
    // UI for system file manager

    Scanner scan = new Scanner(System.in);
    ListFiles list = new ListFiles();
    String input = "";

    while(!input.equals("q")){
      // Show Main Menu
      System.out.println("What do you want to do?");
      System.out.println("Type 'ls' and hit return to list files in this directory");
      System.out.println("Type 'ext' and hit return to list files by extension");
      System.out.println("Type 'txt' to manipulate .txt file");
      System.out.println("Type 'q' and hit return to exit.");
      
      // Get input and execute acoordingly
      input = scan.next();

      if (input.equals("ls")){
        // Call to list files in folder
        list.findFiles();
      } else if (input.equals("ext")) {
        // Ask for extension and call to show files with extension
        System.out.println("Type the extension (without .): ");
        String ext = scan.next();
        list.findExtension(ext);
      } else if (input.equals("txt")) {
        // .txt manipulation menu
        System.out.println("Type '1' and hit enter to see file info");
        System.out.println("Type '2' and hit enter to search for a word");
        String choice = scan.next();
        
        if (choice.equals("1")) {
          // Call to print .txt file info
          list.printFileInfo();
        } else if (choice.equals("2")) {
          // Ask for word and call to search for word
          System.out.println("Type a word: ");
          input = scan.next();
          list.searchForWord(input);
        }
      }
      else if (!input.equals("q")) {
        // Invalid input
        System.out.println("'" + input + "'" + "is not a valid option");
      }
    }
    scan.close();
  }
}