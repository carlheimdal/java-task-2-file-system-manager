package filemanipulation;

import java.io.FileWriter;

public class Log{

  private String dir = "../log/log.txt";

  public boolean logEvent(String entry){
    // Appends the String 'entry' to file 'dir'.
    boolean entryWritten = false;
    try {
      FileWriter f = new FileWriter(dir, true);
      f.append(entry);
      f.close();
      entryWritten = true;
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    return entryWritten;
  }
}