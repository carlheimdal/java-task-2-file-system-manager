package filemanipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
import java.time.LocalDateTime;

public class ListFiles{
  /* Class contrains methods for file manipulation
  Each call to method is logged with:
    - timestamp
    - output
    - execution time
  */
  private String dir = "../Files";
  private File f = new File(dir);
  private Log log = new Log();

  // List all files in directory
  public void findFiles(){
    this.log.logEvent(LocalDateTime.now() + "\r\n");
    long start = System.currentTimeMillis();
    String[] pathnames = f.list();
    System.out.println("Files in directory:");
    this.log.logEvent("Files in directory:\r\n");

    for(String pathname: pathnames){
      System.out.println(pathname);
      log.logEvent(pathname + "\r\n");
    }

    long stop = System.currentTimeMillis();
    this.log.logEvent("time spent: " + (stop - start) + "ms\r\n");
  }

  // List files with specific extension
  public void findExtension(String ext){
    this.log.logEvent(LocalDateTime.now() + "\r\n");
    long start = System.currentTimeMillis();
    this.log.logEvent("." + ext + "files in directory:\r\n");
    String[] pathnames = f.list();
    System.out.println(ext + " files in directory:");
    for(String pathname: pathnames){
      String[] split = pathname.split("\\.");
      if(split[1].compareTo(ext) == 0){
        System.out.println(pathname);
        this.log.logEvent(pathname + "\r\n");
      }
    }
    long stop = System.currentTimeMillis();
    this.log.logEvent("time spent: " + (stop - start) + " ms\r\n");
  }

  // .txt file functions

  // Print file name etc.
  public void printFileInfo(){
    long start = System.currentTimeMillis();
    String dir = "../Files/";
    String dracula = "Dracula.txt";
    System.out.println("Filename: " + dracula);
    System.out.println("File size: " + dracula.length());
    this.log.logEvent("Filename: Dracula.txt\r\n");
    this.log.logEvent("File size: " + dracula.length() + "\r\n");

    try {
      BufferedReader reader = new BufferedReader(new FileReader(dir + dracula));
      int lines = 0;
      while (reader.readLine() != null) lines++;
      reader.close();
      System.out.println("The file has " + lines + " lines.\r\n");
      this.log.logEvent("The file has " + lines + " lines.\r\n");
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    long stop = System.currentTimeMillis();
    this.log.logEvent("time spent: " + (stop - start) + " ms\r\n");
  }

  // Search for word 'word' in Dracula.txt and count occurrences
  public void searchForWord(String word){
    this.log.logEvent(LocalDateTime.now() + "\r\n");
    long start = System.currentTimeMillis();
    int occurrences = 0;
    try {
      Scanner scan = new Scanner(new File("../Files/Dracula.txt"));
      while(scan.hasNextLine()){
        String line = scan.nextLine();
        if(line.toLowerCase().contains(word.toLowerCase())){
          occurrences ++;
        }
      }
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    if(occurrences == 0){
      System.out.println("Could not find word '" + word + "'");
      this.log.logEvent("Could not find word '" + word + "'\r\n");
    } else {
      System.out.println("Found " + occurrences + " occurrences of '" + word + "'");
      this.log.logEvent("Found " + occurrences + " occurrences of '" + word + "'\r\n");
    }
    long stop = System.currentTimeMillis();
    this.log.logEvent("Time spent: " + (stop - start) + " ms.\r\n");
  }

}