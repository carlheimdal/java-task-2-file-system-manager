# Description

Small program for acquiring, printing and logging chosen information
on files in folder Files/

# Compiling

From src/ folder:

$ javac -d ..out/ *.java filemanipulation/*.java

From out/ folder:

$ jar cfe FileSystemManager.jar UserInterface *.class filemanipulation/*.class

# Running

From out/ folder

$ java -jar FileSystemManager.jar

Output is logged in log/log.txt

# Demonstration

![Demonstration](demo.png)